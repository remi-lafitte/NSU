
1)	Lien entre NSU et Verticalité

Prémisses du raisonnement théorique =
1.	Sur le plan comportemental =

-	on retrouve une corrélation entre la présence de NSU extra-personnelle (NEXP; la NSU personnelle (NP) n’étant pas évaluée) et un déficit perceptif dans les tâches de verticale visuelle, haptique, et posturale.
-	Ce déficit de perception de la verticalité est mis en avant dans le plan frontal (rotation en roulis) et sagittal (rotation en tangage). 
-->	Chez ces patients, on a une désorientation spatiale multimodale présente dans le plan horizontal, frontal, et sagittal.

2.	Sur le plan neuro-anatomique = 
i)	perception de la verticale : 
-	un déficit perceptif de la verticale visuelle et haptique est fréquemment retrouvé chez des patients ayant une atteinte hémisphérique au niveau du cortex vestibulaire (insula, operculum pariétal, gyrus temporal supérieur). 
-	Les corrélats anatomiques liés à un déficit de la verticale postural n’ont pas encore été rapportés. 
-->	Il est possible que le cortex vestibulaire soit au centre d’un réseau codant la perception de la verticalité dans plusieurs modalités. Néanmoins il est aussi possible que ce réseau ne prenne pas en charge le traitement de la verticale posturale. Cela dit, on pourrait spéculer que c’est le cas dans la mesure où la verticale haptique est basée sur l’intégration d’informations somesthésiques. Cette possibilité n’est pas claire.

ii)	NSU : 
-	La NSU est associée à des loci lésionnels hétérogènes. L’insula et le gyrus temporal supérieur font partie, entre autres, des loci lésionnels les plus rapportés dans la littérature (Molenberghs, Sale, & Mattingley, 2012).
-	Lorsque l’on compare la NEXP et la NSU personnelle (NP), la NEXP est liée à des lésions ventrales au niveau du lobe temporal, alors que la NP est liée à des lésions plus dorsales au niveau du gyrus post-central et supramarginal (Committerri, 2007). 
-	Committeri (2007) rapporte aussi que des lésions du cortex vestibulaire sont communes à la NEXP et la PN (la corrélation est très forte). 
-->	Il est possible que des lésions du cortex vestibulaire sous-tendent à la fois la NEXP et la NP. Néanmoins, ce résultat n’a été trouvé qu’une seule fois (et rapporté anecdotiquement) et doit donc être confirmé. De plus, la NSU se caractérise globalement par une hétérogénéité lésionnelle très forte. 

3.	Sur le plan théorique = 

-	Selon la littérature, la NP est soit associée à un déficit représentationnel du schéma corporel visuo-spatial/topographique, soit à une atteinte des processus attentionnels portée sur le corps, soit les deux à la fois (Committeri, 2018; Di Vita, 2019). A noter que Vallar (1997) pense que la NP fait suite à une translation de la référence égocentrée vers le côté ipsilésionnel.
-	La NEXP est principalement soit associée à un déficit attentionnel (eg, Corbetta & Shulman, 2011), soit à un biais des traitements spatiaux égocentrés (Karnath & Rorden, 2011). 
-	Alternativement, l’équipe de Kerkhoff (avec Funk, Utz) propose que la NSU pourrait résulter d’une lésion d’un réseau spatial complexe, avec en son “coeur” le cortex vestibulaire (complexe parieto-insulo vestibulaire), chargé de représenter l’espace en 3D. Pour cela, ce réseau intégrerait plusieurs information sensorielles. 
Citations  = 
-	“... systematic tilts of the coordinate systems can be caused by damage to various parts of a complex system underlying the representation of space, including lesions of the central vestibular pathways (brain stem, thalamus, or vestibular cortex),as well as sensory pathways and (right) parietal lesions (as suggested, e.g., by Brandt et al., 1994)” (Funk et al., 2010)
-	“Based on single-cell recordings in the monkey parietal cortex, Sakata et al. (1997) identified neurons in the lateral bank of the caudal intraparietal sulcus which are relevant for the coding of axis orientation in three-dimensional space. Damage to such multimodal and orientation-selective neurons might be responsible for the deficits in the perception and representation of the principal spatial axes (that become manifest in identical tilts in different modalities).” (Funk et al., 2010)
-	“[...] neglect patients show postural (including head-and eye-position) deficits in all spatial planes, which in turn affect the processing of spatial information in all spatial planes.” (Funk et al., 2010)
-	“... the vestibular system also acts in a multispatial way, as it is able to assess the position and movement of our body in three-dimensional space (Brandt and Dieterich, 1999).”  (Utz et al., 2011)
--> Cette hypothèse vestibulaire est compatible avec l’hypothèse d’un “modèle interne de la verticalité”, mis en évidence par les travaux de Barra et Pérennou (eg, Barra et al., 2010). Ce modèle interne de la verticalité est chargé de synthétiser les signaux afférents et efférents afin de percevoir la verticalité et in fine interagir sur le plan moteur dans l’espace (eg, locomotion, posture). 
-->	Cette hypothèse est aussi compatible avec la théorie du “shift” de la référence égocentrée dans la NSU. Bien que critiquée (voir Chokron), cette théorie est soutenur par plusieurs évidences comportementales montrant une translation ipsislésionnelle dans le plan horizontal (Honoré et al., 2009) et une rotation contralésionnelle dans le plan frontal (Barra et al., 2007, 2009).

En résumé = 
--> La NSU est classiquement perçue comme un trouble attentionnel. Aussi, certains chercheurs ont proposé une “hypothèse vestibulaire” de la NSU. Un réseau cortical centré sur le cortex vestibulaire pourrait constituer le corrélat neuro-anatomique du modèle interne de la verticalité. Ce réseau intégrerait différents signaux sensoriels pour générer une représentation multimodale de l’espace, et ceux dans plusieurs dimensions. Une lésion de ce réseau entraînerait une désorientation dans plusieurs plans spatiaux à la fois. Le mécanisme causal à l’oeuvre derrière la NSU pourrait être i) l’induction d’un biais de position à cause de multiples problèmes d’ajustement posturaux, et par conséquent un biais de traitement égocentré; ii) une distorsion de la représentation de la référence égocentrée du corps, qui entraînerait le biais de position. Dans les deux cas, l’hypothèse vestibulaire peut expliquer causalement la NSU en postulant une détérioration des traitements spatiaux égocentrés, tant en terme de biais que de variabilité.  
-->	A noter cependant qu’un déficit perceptif de la verticalité pourrait être corrélé à la NSU sans en être la cause. Séparer le déficit attentionnel (Corbetta & Shulman, 2011) et celui de la perception de la verticale dans la NSU nécessiterait un paradigme spécial à l’avenir (ça n’est pas le but de cette étude). C’est une limite à retenir. 

4.	“Vide” dans la littérature et hypothèses de notre étude = 
-	L’hypothèse d’une représentation multimodale de la verticalité lésée dans la NSU se base essentiellement sur des travaux ayant évalué la verticale visuelle et haptique, mais pas la verticale posturale. 
-	Les corrélats neuro-anatomiques de la verticale posturale n’ont quasiment pas été investi jusqu’ici et nécessitent d’être mis en parallèle avec les corrélats neuro-anatomiques de la verticale visuelle et haptique (eg, Rousseaux et al., 2013).
-	Un déficit multmodal de la verticalité a été mis en évidence chez dans la NEXP, mais pas explicitement dans la NP puisqu’elle n’est généralement pas évaluée. 
-	Or, il est possible que le caractère multimodal du déficit perceptif de la verticalité soit du à des lésions conjointe d’aires ou de réseaux impliqués séparément dans la NEXP et la NP. Par exemple, une lésion du réseau “X” pourrait provoquer un biais de verticale posturale et/ou haptique et être associé à une NP, tandis qu’une lésion du réseau “Y” pourrait provoquer un biais de verticale visuelle et être associée à une NEXP. 
$\to$	En résumé, l’hypothèse vestibulaire (associée à l’hypothèse du modèle interne de la verticalité) postule qu’une lésion d’une représentation multimodale de la verticalité provoquerait causalement la NSU. Pour soutenir ou non cette théorie, notre étude pourrait investir deux hypothèses : 
-	H1 : Si un modèle interne de la verticalité est lésé et sous-tend la NSU, alors on devrait observer un biais dans la verticale posturale et verticale qui va dans la même direction, c’est à dire vers le côté contralésionnel.  
-	H2 : Si H1 est supporté par nos résultats, il serait intéressant à titre exploratoire de chercher si ce déficit multimodal est retrouvé uniquement chez des patients ayant à la fois une NEXP et une NP, comparé à des patients ayant une NEXP ou une NP “pure”. Sur le plan neuro-anatomique, Committeri et al. (2007) a observé des lésions communes entre la NEXP et la NP au niveau du cortex vestibulaire. Les profils de lésions séparant la NEXP et la NP se situaient à l’extérieur du cortex vestibulaire, dans des zones limitrophes. Sur la base de ces résultats, des patients avec une NP ou une NEXP “pure” ne devrait pas être atteints par un déficit multimodal de la verticalité. Certains évidences comportementales (Baas et al., 2011; DiVita et al., 2016) et neuro-anatomiques (Committeri et al., 2007; DiVita et al., 2019; Pérennou et al., 2008) laissent penser qu’une NEXP pure pourrait être associée à un biais de verticale visuelle, tandis qu’une NP pure pourrait être associée à un biais de verticale posturale. La présente étude permettrait d’investir l’hypothèse qu’un déficit de traitement spatial égocentré induit par un dysfonctionnement du modèle interne de la verticalité puisse sous-tendre à la fois la NEXP et la NP (même si encore une fois ce résultat resterait de nature corrélationnelle).
$\to$Point fort de notre étude : la NSU personnelle n’a jamais été prise en compte par l’équipe de Kerkhoff, qui n’a jamais évalué la posturale verticale non plus. En créant un score composite de la NSU basé sur des tests de NSU extra-personnelle ET personnelle, on peut explorer plus fidèlement le lien entre un déficit perceptif multimodal de la verticalité et le phénomène de négligence spatiale.
-->	Point faible : on risque d’avoir très peu de patients avec un profil de NSU personnelle “pure”, et donc un manque de puissance statistique. Malheureusement, on ne peut pas utiliser le degré de NSU personnelle en variable continue pour tester l’hypothèse 2, car cette variable covarie probablement avec le degré de NSU extra-personnelle
-->	Point non résolu : Le mécanisme exact par lequel une lésion entraînerait un biais spatial dans le plan horizontal (NSU) et frontal est toujours flou. Est ce que, comme le pense Funk et al (2010), une distorsion de la perception de l’espace en 3D induit un problème de posture (biais de position), et donc en retour une “orientation spatiale inconstante” , et donc une NSU ? Notre étude ne répond pas à cette question. Si tel est le cas, l’adaptation prismatique, connue pour recalibrer les traitements spatiaux égocentrés dans le plan horizontal, devrait aussi recalibrer la perception de la verticalité dans la NSU. 


5.	Plan statistique = 
•	H1 (hypothèse “forte”) : On teste 3 corrélations : 
-	a) vertical visuelle ~ score de NSU (VI continue)
-	b) vertical posturale ~ score de NSU (VI continue)
-	c) verticale visuelle ~ verticale posturale 
--> On veut a) confirmer la corrélation entre un biais de verticale visuelle et le degré de NSU ; et b) de façon innovante montrer que cette corrélation se retrouve aussi dans la verticale posturale et va dans le même sens. c) devrait soutenir qu’un déficit perceptif de la verticalité dans 2 modalités différentes est liée à la lésion d’une représentation multimodale. 
--> A titre exploratoire, si l’on veut tester la différence de corrélation entre la verticale visuelle et posturale, il faudra recourir à un “test d’équivalence”, qui permet de conclure avce plus de robustesse qu’un test classique sur une absence de différence (H0). 
$\to$	Les variables dépendantes  pourraient être la moyenne (biais) et l’écart type (si données disponibles).

•	H2 (hypothèse “faible”, plus exploratoire):
•	OPTION 1 “dirty” = On crée 3 groupes NEXP, NP, et NEXP+NP sur la base de cut-offs. Pour chaque groupe on teste 2 corrélations : 
-	vertical visuelle ~ score de NEXP (VI continue)
-	vertical posturale ~ score de NEXP (VI continue)
-	vertical visuelle ~ score de NP (VI continue)
-	vertical posturale ~ score de NP (VI continue)
-	vertical visuelle ~ score de NEXP+NP (VI continue)
-	vertical posturale ~ score de NEXP+NP (VI continue)
•	OPTION 2 plus “fine” = On crée 3 groupes NEXP, NP, et NEXP+NP sur la base de cut-offs. On teste différentes contrates en interaction avec la modalité de la tâche de verticalité = 
-	contraste1 = - 0.5(NEXP+NP) + 0.25(NP) + 0.25(NEXP)
-	contraste2 = 0(NEXP+NP) - 0.5(NP) + 0.5(NEXP)
-	biais/SD ~ contraste1 + contraste2 + modalité + contraste1:modalité + contraste2:modalité
-->	effet principal de modalité = pas important
-->	effet principal de contraste1 = très important, il teste si le biais, quelque soit la modalité, est supérieur quand NEXP+NP vs NP ou NEXP seules. 
-->	effet principal de contraste2 = pas important, devrait être non significatif. 
-->	effet d’interaction de contraste1:modalité = important, devrait être non significatif, quelque soit la modalité le biais dans le groupe NEXP+NP reste plus fort vs NP ou NEXP seules. 
--> effet d’interaction de contraste2:modalité = important sur le plan exploratoire, pour tester si NEXP seule est plus liée à un biais de verticale visuelle comparée à NP seule, et si NP seule est plus liée à un biais de verticale posturale comparée à NEXP seule.
•	H3 = Sur le plan neuro-anatomique, on pourrait constituer plusieurs groupes selon cut-offs : 
-	VP+/VP- selon si la verticale posturale est pathologique comparée à une norme contrôle.
-	VV+/VV- selon si la verticale visuelle est pathologique comparée à une norme contrôle.
-	NEXP+NP / NP / NEXP 
-->	On s’attend à overlap lésionnel entre NEXP+NP/VV+/VP+ 
-->	A titre exploratoire, on s’attend à overlap lésionnel entre NEXP/VV+
-->	A titre exploratoire, on s’attend à overlap lésionnel entre NP/VP+ 

6.	Synthèse =  Sur le plan comportemental, neuro-anatomique, théorique, vide dans la littérature, hypothèse, et stats =
-	Chez les patients atteints de NSU, on observe une désorientation spatiale multimodale présente dans le plan horizontal, frontal, et sagittal.
-	Or, il est possible que le cortex vestibulaire soit au centre d’un réseau codant la perception de la verticalité dans plusieurs modalités. Néanmoins il est aussi possible que ce réseau ne prenne pas en charge le traitement de la verticale posturale. 
-	La NSU est associée à des loci lésionnels hétérogènes. L’insula et le gyrus temporal supérieur font partie, entre autres, des loci lésionnels les plus rapportés dans la littérature (Molenberghs, Sale, & Mattingley, 2012). Egalement, il est possible que des lésions du cortex vestibulaire sous-tendent à la fois la NEXP et la NP. Néanmoins, ce résultat n’a été trouvé qu’une seule fois (rapporté anecdotiquement par Commietteri, 2007).
-	Certains chercheurs ont donc proposé une “hypothèse vestibulaire” de la NSU. Un réseau cortical centré sur le cortex vestibulaire pourrait constituer le corrélat neuro-anatomique du modèle interne de la verticalité. Une lésion de ce réseau entraînerait une désorientation dans plusieurs plans spatiaux à la fois. Le mécanisme causal à l’oeuvre derrière la NSU pourrait être i) l’induction d’un biais de position à cause de multiples problèmes d’ajustement posturaux, et par conséquent un biais de traitement égocentré; ii) une distorsion de la représentation de la référence égocentrée du corps, qui entraînerait le biais de position. Dans les deux cas, l’hypothèse vestibulaire peut expliquer causalement la NSU en postulant une détérioration des traitements spatiaux égocentrés, tant en terme de biais que de variabilité.  
-	Néanmoins, l’hypothèse d’une représentation multimodale de la verticalité lésée dans la NSU se base essentiellement sur des travaux ayant évalué la verticale visuelle et haptique, mais pas la verticale posturale. 
-	De plus, un déficit multimodal de la verticalité a été mis en évidence chez dans la NEXP, mais pas explicitement dans la NP puisqu’elle n’est généralement pas évaluée.
-	Afin de répondre à ces deux problématiques, nous évaluerons deux hypothèses : 
-	H1 : Si un modèle interne de la verticalité est lésé et sous-tend la NSU, alors on devrait observer un biais dans la verticale posturale et verticale qui va dans la même direction, c’est à dire vers le côté contralésionnel.  
-	H2 : Ce déficit multimodal devrait être retrouvé uniquement chez des patients ayant à la fois une NEXP et une NP, comparé à des patients ayant une NEXP ou une NP “pure”. 
-	H3 : L’hypothèse 2 pourrait être soutenue sur le plan neuro-anatomique en observant le recouvrement lésionnel entre les aires lésées chez les patients NEXP+NP et celles lésées chez les patients ayant un déficit conjoint dans la verticale visuelle et posturale.
-	Plan statistique = 
•	H1 (hypothèse “forte”) : 
-	a) vertical visuelle ~ score de NSU (VI continue)
-	b) vertical posturale ~ score de NSU (VI continue)
-	c) verticale visuelle ~ verticale posturale 
•	H2 (hypothèse plus exploratoire) : 
•	On crée 3 groupes NEXP, NP, et NEXP+NP sur la base de cut-offs. On teste différents contrates en interaction avec la modalité de la tâche de verticalité = 
-	contraste1 = - 0.5(NEXP+NP) + 0.25(NP) + 0.25(NEXP)
-	contraste2 = 0(NEXP+NP) - 0.5(NP) + 0.5(NEXP)
-	biais/SD ~ contraste1 + contraste2 + modalité + contraste1:modalité + contraste2:modalité
•	H3 = Sur le plan neuro-anatomique, on s’attend à un overlap lésionnel entre les groupes NEXP+NP/VV+/VP+ 
